const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Product is required"]
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	price : {
		type : Number,
		required : [true, "Price is required"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	orderProduct : [
			{
				productId : {
					type : String,
					required : [true, "UserId is required"]
				},
				orderedOn : {
					type : Date,
					default : new Date()
				}
			}

		]
})

module.exports = mongoose.model("Product", courseSchema);